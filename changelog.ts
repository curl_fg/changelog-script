#!/usr/bin/env tsx
import { exec } from 'node:child_process';
import { readFileSync, writeFileSync } from 'node:fs';

const withBang = 'Breaking changes',
  withoutCtx = 'Others',
  changesFile = './Changelog.md',
  defaultScore = '0.0.0',
  weight: Record<string, typeof lastVersion> = {
    build: '0.0.0',
    chore: '0.0.0',
    config: '0.0.0',
    feat: '0.1.0',
    fix: '0.0.1',
    refactor: '0.1.0',
    release: '1.0.0',
    test: '0.0.0'
  };

const lastHashAndVer = /<!-+\s*(?<hash>[^:]+):(?<version>.+)\s*-+>$/;

let lastVersion: `${number}.${number}.${number}` = '0.0.0',
  lastHash: string | undefined;

try {
  const content = readFileSync(changesFile, 'utf-8').match(lastHashAndVer);

  lastHash = content?.groups?.hash;
  lastVersion = content?.groups?.version as typeof lastVersion;
} catch (_err) {}

(async () => {
  await simpleCmd('git --version');

  exec(
    `git log --no-decorate ${lastHash ? `${lastHash}..` : ''}HEAD`,
    async (error, stdout, stderr) => {
      if (error || !/^\s*$/.test(stderr))
        throw new Error(`${error?.message ?? error}. ${stderr}`);

      const data = dissectGitLog(stdout).filter(
          ({ type }) => type !== withoutCtx
        ),
        version = getVersion(data, lastVersion);

      if (data.length === 0) {
        console.log('No new commits 👌');
        return;
      }

      const commitsGrouped = [
        ...groupBy(data, ({ type }) =>
          type.replace(/^[^!]+!$/, withBang)
        ).entries()
      ].map(([type, commits]) => {
        const ctxs = [
          ...groupBy(
            commits,
            ({ context }) => context,
            ({ message, hash }) => {
              const [head, ...parts] = message.split('\n');
              return `[${head}](../../../commit/${hash})${parts.join('\n')}`;
            }
          ).entries()
        ];

        return [type, ctxs] as [typeof type, typeof ctxs];
      });

      const body = commitsGrouped.reduce((acc, [title, ctxAndMsgs]) => {
        return `${acc}## ${capitalize(title)}\n${ctxAndMsgs
          .map(
            ([ctx, msgs]) =>
              `### ${capitalize(ctx.replace(/^\s*$/, 'Others'))}\n${msgs
                .map(c => `* ${c}`)
                .join('\n')}\n`
          )
          .join('\n')}\n`;
      }, '');

      let oldContent = '';

      try {
        oldContent = readFileSync(changesFile, 'utf-8').replace(
          lastHashAndVer,
          ''
        );
      } catch (_err) {}

      writeFileSync(
        changesFile,
        `# Version: ${version}\n${body}${oldContent}<!-- ${data[0].hash}:${version} -->`
      );

      try {
        await simpleCmd(`git tag v${version}`);
      } catch (_err) {
        console.error(
          `The v${version} tag wasn't created since it already exists`
        );
      }

      console.log('Changelog.md updated 👌');
    }
  );
})();

// #region Functions c:

function getVersion(
  data: ReturnType<typeof dissectGitLog>,
  version: typeof lastVersion
) {
  return data.reduce<typeof version>((acc, { score }) => {
    const [accMayor, accMinor, accPatch] = acc.split('.'),
      [mayor, minor, patch] = score.split('.');

    return `${Number(accMayor) + Number(mayor)}.${
      Number(accMinor) + Number(minor)
    }.${Number(accPatch) + Number(patch)}`;
  }, version);
}

function dissectGitLog(log: string) {
  const commitMsg =
    /^commit (?<hash>[a-z0-9]+)$\n(?:^.*$\n){3}(?<fullMessage>(?:^\s+.*$\n){1,})$/gm;

  const fullMessage = /(?<fullContext>[^:]+):\s*(?<message>(?:.*$\n?)+)/m,
    ctxAndType = /(?<type>[^(]+)\((?<context>[^)]+)?\)/;

  return [...log.matchAll(commitMsg)].map(({ groups }) => {
    const hash = groups?.hash;

    const fullMsg =
      groups?.fullMessage
        .split(/\n+/)
        .map(str => str.trim())
        .join('\n') ?? '';

    let { message = fullMsg, fullContext = withoutCtx } =
      fullMsg.match(fullMessage)?.groups ?? {};

    let { type = null, context = null } =
      fullContext.match(ctxAndType)?.groups ?? {};

    message = message.trim();
    context = context ?? '';
    type = type ?? fullContext;

    return {
      context,
      hash,
      message,
      score: weight[type] ?? defaultScore,
      type
    };
  });
}

function capitalize(str: string) {
  if (str === '') return '';

  return `${str[0].toUpperCase()}${str.slice(1).toLowerCase()}`;
}

async function simpleCmd(cmd: string) {
  return new Promise<string>((res, rej) => {
    exec(cmd, (error, stdout, stderr) => {
      if (error || !/^\s*$/.test(stderr))
        rej(new Error(`${error?.message ?? error}. ${stderr}`));
      else res(stdout);
    });
  });
}

function groupBy<T>(iter: Iterable<T>): Map<T, T[]>;
function groupBy<T>(iter: Iterable<T>, keyFx: undefined): Map<T, T[]>;

function groupBy<T, K>(iter: Iterable<T>, keyFx: (v: T) => K): Map<K, T[]>;

function groupBy<T, V>(
  iter: Iterable<T>,
  keyFx: undefined,
  valFx: (v: T) => V
): Map<T, V[]>;

function groupBy<T, K, V>(
  iter: Iterable<T>,
  keyFx: (v: T) => K,
  valFx: (v: T) => V
): Map<K, V[]>;

function groupBy<T>(
  iter: Iterable<T>,
  keyFx: (v: T) => T = itm => itm,
  valFx: (v: T) => T = itm => itm
): Map<T, T[]> {
  const mapped: Array<[T, T]> = [];

  for (const itm of iter) mapped.push([keyFx(itm), valFx(itm)]);

  return mapped.reduce((acc, [key, val]) => {
    const old = acc.get(key);

    if (old === undefined) acc.set(key, [val]);
    else acc.set(key, [...old, val]);

    return acc;
  }, new Map<T, T[]>());
}
// #endregion Functions c:
