# Changelog script
Creates a file with the change history of a git repository. This is created
from the commit messages, it is recommended to use conventional commits.