# Version: 0.1.1
## Fix
### Others
* [Now compatible with multiple line message](../../../commit/5b1f7aa34e22117baabb765dd7e7c9326629f23e)

## Feat
### Others
* [Adds Hash link](../../../commit/d0c583685703b8b0ee028894238f2ce6c9126352)

<!-- 5b1f7aa34e22117baabb765dd7e7c9326629f23e:0.1.1 -->